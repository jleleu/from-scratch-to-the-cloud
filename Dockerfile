FROM golang:1.14.2 as builder

RUN go env -w GOBIN=/go/bin

# Create and change to the app directory
WORKDIR /app

# Copy local code to the container image
COPY . ./

# Get dependencies
RUN go get

# Build the binary
RUN CGO_ENABLED=0 GOOS=linux go build -v -o from-scratch-to-the-cloud

# Use the official Alpine image for a lean production container
FROM alpine:3
RUN apk add --no-cache ca-certificates

# Copy the binary to the production image from the builder stage
COPY --from=builder /app/from-scratch-to-the-cloud /from-scratch-to-the-cloud

# Run the web service on container startup
CMD ["/from-scratch-to-the-cloud"]