# Makefile

GCP_PROJECT = from-scratch-to-the-cloud
IMAGE_NAME = from-scratch-to-the-cloud
GIT_TAG = "1.0.0"
IMAGE_VERSION := $(if $(GIT_TAG),$(GIT_TAG),latest)

help: ## print this message
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[31m%-10s\033[0m\t%s\n", $$1, $$2}'

version: ## show the image version
	@echo "$(IMAGE_NAME):$(IMAGE_VERSION) , git tag : $(GIT_TAG)"

docker-build: ## create the image
	docker build --no-cache -t $(IMAGE_NAME):latest .

docker-push: build ## build and push image
	docker tag $(IMAGE_NAME):latest $(IMAGE_NAME):$(IMAGE_VERSION)
	docker push $(IMAGE_NAME):$(IMAGE_VERSION)

docker-run: ## create a new container from the image
	docker run -p 8080:8080 $(IMAGE_NAME):latest

docker-pull: ## pull the image from the registry
	docker pull $(IMAGE_NAME):latest

gcloud-push: ## push image to Google Container Registry
	gcloud builds submit --tag gcr.io/$(GCP_PROJECT)/$(IMAGE_NAME)

gcloud-pull: ## pull image from Google Container Registry
	docker pull gcr.io/$(GCP_PROJECT)/$(IMAGE_NAME):latest

gcloud-deploy: ## deploy to Google Cloud Run
	gcloud run deploy --image gcr.io/$(GCP_PROJECT)/$(IMAGE_NAME) --platform managed