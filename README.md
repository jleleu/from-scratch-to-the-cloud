# from-scratch-to-the-cloud

This project aims to be a training platform, on which I will build an application from scratch using the current products of the IT market.

1. TDD approach
2. MongoDB
3. API (YAML, Swagger Editor)
4. Git, Gitlab
5. Jenkins for the CI/CD Pipeline
6. Docker
7. GCP, Cloud Run, Kubernetes Engine
