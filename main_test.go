package main

import "testing"

func TestSayMessage(t *testing.T) {
	expected := "Nice, connected to MongoDB!"
	if observed := sayMessage(); observed != expected {
		t.Fatalf("SayMessage() = %v, want %v", observed, expected)
	}

}
